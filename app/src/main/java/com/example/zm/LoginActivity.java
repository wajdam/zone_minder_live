package com.example.zm;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;

public class LoginActivity extends
        AppCompatActivity {

    private static final String url = "http://10.10.0.100:8000/monitors";
    public static JSONObject monitors = new JSONObject();
    public static boolean logged_in = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final RequestQueue queue = Volley.newRequestQueue(this);

        CookieManager manager = new CookieManager();
        CookieHandler.setDefault( manager  );

        final JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                null,
                new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response){
                monitors = response;
                logged_in = true;
                JSONArray names = response.names();
                for (int i = 0; i < names.length(); i++) {

                    try {
                        Log.i("NameLoginACT", names.get(i).toString());
                        Log.i("ValueLoginACT", response.getString(names.get(i).toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d("Error", error.getMessage());
                Toast.makeText(LoginActivity.this, "ERROR 404, try again!", Toast.LENGTH_SHORT).show();
            }

        }){
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";

            }

            @Override
            public byte[] getBody() {
                final EditText user = findViewById(R.id.login);
                final EditText pass = findViewById(R.id.password);
                String body = "user="+user.getText()+"&pass="+pass.getText();
                return body.getBytes();
            }
        };


        final Button button = findViewById(R.id.submit);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                queue.add(objectRequest);
            }
        });

    }
}

