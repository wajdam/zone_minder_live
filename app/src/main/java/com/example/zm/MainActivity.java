package com.example.zm;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity implements
        AdapterView.OnItemClickListener {

    private static final ArrayList<String> items = new ArrayList<String>();
    private static final ArrayList<String> items_names = new ArrayList<String>();
    public static String cameraUrl = "";
    public GridView grid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        items.clear();

        grid = findViewById(R.id.grid);
        grid.setAdapter(new CustomGridAdapter(this, items));
        grid.setOnItemClickListener(this);

        if (!LoginActivity.logged_in) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }else{
            JSONObject items_objects = LoginActivity.monitors;
            JSONArray names = items_objects.names();
            for (int i = 0; i < names.length(); i++) {
                try {
                    Log.i("Name", names.get(i).toString());
                    items_names.add(names.get(i).toString());
                    Log.i("Value", items_objects.getString(names.get(i).toString()));
                    items.add(items_objects.getString(names.get(i).toString()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub
        Toast.makeText(MainActivity.this, "Clicked position is" + arg2,
                Toast.LENGTH_LONG).show();
    }

    public class CustomGridAdapter extends BaseAdapter {
        private Activity mContext;

        // Keep all Images in array
        public ArrayList<String> mThumbIds;
        private int  counter = 0;

        // Constructor
        public CustomGridAdapter(MainActivity mainActivity, ArrayList<String> items) {
            this.mContext = mainActivity;
            this.mThumbIds = items;

        }

        @Override
        public int getCount() {
            return mThumbIds.size();
        }

        @Override
        public Object getItem(int position) {
            return mThumbIds.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflator = mContext.getLayoutInflater();
            if(convertView==null)
            {
                convertView = inflator.inflate(R.layout.grid_layout, null);

            }
            TextView text = (TextView) convertView.findViewById(R.id.text);
            VideoView videoView = (VideoView) convertView.findViewById(R.id.image);
            text.setText(items_names.get(position));
            MediaController mediaController = new MediaController(MainActivity.this);
            mediaController.setAnchorView(videoView);
            videoView.setMediaController(mediaController);
            videoView.setVideoPath(mThumbIds.get(position));
            videoView.setForegroundGravity(Gravity.CENTER);
            videoView.requestFocus();
            videoView.setZOrderOnTop(false);
            videoView.postInvalidateDelayed(0);
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Log.i("video", "setOnErrorListener");
                    return true;
                }
            });
            videoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("LOG_TAG", mThumbIds.get(position));
                    cameraUrl = mThumbIds.get(position);
                    startActivity(new Intent(MainActivity.this, VideoActivity.class));
                    finish();
                }
            });
            videoView.start();

            return convertView;
        }

    }
}
