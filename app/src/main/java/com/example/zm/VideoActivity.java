package com.example.zm;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

public class VideoActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        if (!LoginActivity.logged_in) {
            startActivity(new Intent(VideoActivity.this, LoginActivity.class));
            finish();
        }else {
            VideoView videoView = (VideoView) findViewById(R.id.videoView);
            MediaController mediaController = new MediaController(this);
            mediaController.setAnchorView(videoView);
            videoView.setVideoPath(MainActivity.cameraUrl);
            videoView.setZOrderOnTop(false);
            videoView.postInvalidateDelayed(0);
            videoView.start();
        }
}
    @Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        startActivity(new Intent(VideoActivity.this, MainActivity.class));
        finish();
    }
}
